package br.com.alura.leilao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LeilaoApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(LeilaoApplication.class, args);
		
        
	}

}
